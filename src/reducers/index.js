import { combineReducers } from 'redux'
import bookReducer from './bookReducer'
import purchaseSummaryReducer from './purchaseSummaryReducer'

export default combineReducers({
  books: bookReducer,
  purchaseSummary: purchaseSummaryReducer
})
