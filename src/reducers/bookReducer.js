import {
  FETCH_BOOKS_FAILURE,
  FETCH_BOOKS_REQUEST,
  FETCH_BOOKS_SUCCESS,
  FETCH_POSTS,
  NEW_POST,
  SET_CURRENT_RECORD_ID,
  SET_FILTERS,
  SET_SORTING_DICT
} from '../actions/types'

const initialState = {
  isFetching: false,
  error: undefined,
  sortingDict: {
    title: undefined,
    author: undefined
  },
  items: ['aa', 'bb'],
  item: {},
  filterOptions: {
    titleFilter: '',
    authorFilter: ''
  },
  currentId: undefined
}

export default function (state = initialState, action) {
  switch (action.type) {
    case FETCH_POSTS:
      return {
        ...state,
        items: action.payload,
        isFetching: true
      }

    case FETCH_BOOKS_REQUEST:
      return {
        ...state,
        isFetching: true,
        error: undefined
      }

    case FETCH_BOOKS_SUCCESS:
      return {
        ...state,
        isFetching: false,
        items: action.payload
      }

    case FETCH_BOOKS_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: action.error
      }

    case NEW_POST:
      return {
        ...state,
        item: action.payload
      }

    case SET_FILTERS:
      return {
        ...state,
        filterOptions: {
          ...state.filterOptions, ...action.payload
        }
      }

    case SET_SORTING_DICT:
      return {
        ...state,
        sortingDict: { ...state.sortingDict, ...action.payload }
      }

    case SET_CURRENT_RECORD_ID:
      return {
        ...state,
        currentId: action.payload
      }

    default:
      return state
  }
}
