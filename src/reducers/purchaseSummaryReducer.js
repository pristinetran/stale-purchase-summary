import {
  SET_PRICING,
  FETCH_PRODUCT_REQUEST,
  FETCH_PRODUCT_SUCCESS,
  FETCH_PRODUCT_FAILURE, SET_PROMO_CODE
} from '../actions/types'

const initialState = {
  isFetching: false,
  error: undefined,
  product: {},
  promoCode: ''
}

export default function (state = initialState, action) {
  switch (action.type) {
    case SET_PRICING:
      return {
        ...state,
        product: { ...state.product, pricing: action.payload }
      }

    case SET_PROMO_CODE:
      return {
        ...state,
        promoCode: action.payload
      }

    case FETCH_PRODUCT_REQUEST:
      return {
        ...state,
        isFetching: true,
        error: undefined
      }

    case FETCH_PRODUCT_SUCCESS:
      return {
        ...state,
        isFetching: false,
        product: action.payload
      }

    case FETCH_PRODUCT_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: action.error
      }

    default:
      return state
  }
}
