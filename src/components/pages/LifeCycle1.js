import React from 'react'
import PropTypes from 'prop-types'

const LifeCycle1 = ({ books }) => {
  return (
    <div>
      <h1>LifeCycle 1</h1>
      {books && books.map((book, key) => <h5 key={key}>{book.title}</h5>)}
    </div>
  )
}

LifeCycle1.propTypes = {
  books: PropTypes.array
}

export default LifeCycle1
