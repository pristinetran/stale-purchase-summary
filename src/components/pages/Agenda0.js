/// having to import and invoke useState many times as many properties declared in initialState object
/// anti-pattern, full of distraction
import React, { useState, Fragment } from 'react'
import { Col, Form, FormGroup, Input, Label } from 'reactstrap'
import merge from 'lodash/merge'

/// bearing the initialState in this View/Presentation layer, hindering focus and SoC objective
const initialState = {
  count: 0,
  step: 1,
  profile: {
    firstName: 'John',
    lastName: 'Doe',
    age: 36
  }
}

const Agenda0 = (props) => {
  /// Spaghetti code with sandwiched logic - undesirable
  const [count, setCount] = useState(initialState.count)
  const [profile, setProfile] = useState(initialState.profile)

  // Try useState('String url'), and useState(Boolean bool)
  // eslint-disable-next-line
  const [url, setUrl] = useState('http://hn.algolia.com/api/v1/search?query=redux')

  // eslint-disable-next-line
  const [bool, setBool] = useState(false)

  //  Conclusion: we can rather define
  //  initialState = {   count: 0,
  //    step: 1,
  //    url: 'http://hn.algolia.com/api/v1/search?query=redux',
  //    bool: false
  //  } and it will work the same way.
  // That will keep Component slimmer, DOM template only, the Component becomes a less-to-be-worried entity
  // and everything in the app homogeneous with same way/pattern in Container.
  // Code implementation with multiple useState, useEffect introduce fragments, variations and challenge in code reading time, review and maintenance effort.

  // Try useState('redux')
  // eslint-disable-next-line
  const [books, setBook] = useState('redux')

  const onAdd = () => setCount(c => c + initialState.step)
  const handleChange = (e, profile, field) => {
    e.preventDefault()
    setProfile(merge({}, profile, {
      [field]: e.target.value
    }))
  }

  console.warn('Url >>>', url, 'Bool >>>', bool)

  console.warn('Redux >>>', books)
  // setTimeout(() => setBook(['eee', 'fff', 'ggg'], 2000))

  // Conclusion: unless useReducer is used, which is unnecessary, this cannot work
  // Hits the WarWarning: State updates from the useState() and useReducer() Hooks don't support the second callback argument. To execute a side effect after rendering, declare it in the component body..
  // that is undesirable
  // We'd rather use useEffect which is lifecycle in the Container.
  // The utils functions: withStateHandlers and lifecycle should be sufficient for most use cases, to keep things organized in the Container and allow all can be passed as props to the JSX renderer.

  /// The Agenda0 View should only have template code (of DOM elements)
  return (
    <Fragment>
      <button onClick={onAdd}>{count}</button>
      <Form>
        <FormGroup row>
          <Label for='firstName' sm={2}>First Name</Label>
          <Col sm={10}>
            <Input value={profile.firstName} onChange={e => handleChange(e, profile, 'firstName')} type='text' id='firstName' />
          </Col>
        </FormGroup>

        <FormGroup row>
          <Label for='lastName' sm={2}>Last Name</Label>
          <Col sm={10}>
            <Input value={profile.lastName} onChange={e => handleChange(e, profile, 'lastName')} type='text' id='lastName' />
          </Col>
        </FormGroup>
      </Form>
    </Fragment>
  )
}

export default Agenda0
