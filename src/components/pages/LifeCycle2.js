import React from 'react'
import PropTypes from 'prop-types'

const LifeCycle2 = ({ books }) => {
  return (
    <div>
      <h1>LifeCycle 2</h1>
      {books && books.map((book, key) => <h5 key={key}>{book.title}</h5>)}
    </div>
  )
}

LifeCycle2.propTypes = {
  books: PropTypes.array
}

export default LifeCycle2
