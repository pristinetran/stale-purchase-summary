import React from 'react'
import PropTypes from 'prop-types'

const LifeCycle3 = ({ items }) => {
  return (
    <div>
      <h1>LifeCycle3</h1>
      {items && items.map((book, key) => <h5 key={key}>{book.title}</h5>)}
    </div>
  )
}

LifeCycle3.propTypes = {
  items: PropTypes.array
}

export default LifeCycle3
