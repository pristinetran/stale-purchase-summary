import React, { useState, useEffect } from 'react'
import { sampleGetBooks } from '../../utils'
import isEmpty from 'lodash/isEmpty'

const LifeCycle0 = () => {
  const [books, setBooks] = useState([])
  useEffect(() => {
    if (isEmpty(books)) {
      sampleGetBooks(setBooks)
    }
  })

  return (
    <div>
      <h1>LifeCycle 0</h1>
      {books && books.map((book, key) => <h5 key={key}>{book.title}</h5>)}
    </div>
  )
}
export default LifeCycle0
