import React from 'react'
import BooksDirective from '../../containers/BooksDirective'
import BookForm from '../book/BookForm'

const Book = () => {
  return (
    <div>
      <h2>Book Store</h2>
      <BooksDirective />
      <BookForm />
    </div>
  )
}
export default Book
