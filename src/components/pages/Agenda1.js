import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { Col, Form, FormGroup, Input, Label } from 'reactstrap'

const Agenda1 = props => {
  const { onAdd, count, profile = {}, setProfile } = props
  return (
    <Fragment>
      <button onClick={onAdd}>{count}</button>
      <Form>
        <FormGroup row>
          <Label for='firstName' sm={2}>First Name</Label>
          <Col sm={10}>
            <Input value={profile.firstName} onChange={(e) => setProfile(e, { firstName: e.target.value })} type='email' name='email' id='firstName' placeholder='enter name' />
          </Col>
        </FormGroup>

        <FormGroup row>
          <Label for='lastName' sm={2}>Last Name</Label>
          <Col sm={10}>
            <Input value={profile.lastName} onChange={(e) => setProfile(e, { lastName: e.target.value })} type='email' name='email' id='lastName' placeholder='enter name' />
          </Col>
        </FormGroup>
      </Form>
    </Fragment>
  )
}

Agenda1.propTypes = {
  onAdd: PropTypes.func,
  count: PropTypes.number,
  profile: PropTypes.object,
  setProfile: PropTypes.func
}

export default Agenda1
