import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { Col, Form, FormGroup, Input, Label } from 'reactstrap'
import merge from 'lodash/merge'

const Agenda3 = ({ onAdd, count, profile, setProfile, xyz, items }) => (
  <Fragment>
    <p>{xyz}</p>
    <p> These items are from Redux store {JSON.stringify(items, null, 4)}</p>
    <button onClick={onAdd}>{count}</button>
    <Form>
      <FormGroup row>
        <Label for='firstName' sm={2}>First Name</Label>
        <Col sm={10}>
          <Input value={profile.firstName} onChange={(e) => {
            e.preventDefault()
            setProfile(merge(profile, { firstName: e.target.value }))
          }} type='email' name='email' id='firstName' placeholder='enter name' />
        </Col>
      </FormGroup>
      <FormGroup row>
        <Label for='lastName' sm={2}>Last Name</Label>
        <Col sm={10}>
          <Input value={profile.lastName} onChange={(e) => {
            e.preventDefault()
            setProfile(merge(profile, { lastName: e.target.value || '' }))
          }} type='email' name='email' id='lastName' placeholder='enter name' />
        </Col>
      </FormGroup>
    </Form>
  </Fragment>
)

Agenda3.propTypes = {
  onAdd: PropTypes.func,
  setProfile: PropTypes.func,
  profile: PropTypes.object,
  xyz: PropTypes.string,
  items: PropTypes.items,
  count: PropTypes.number
}

export default Agenda3
