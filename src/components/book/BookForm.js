import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { createBook } from '../../actions/actionCreators'
import { Button, Form, FormGroup, Label, Input } from 'reactstrap'

class BookForm extends Component {
  constructor (props) {
    super(props)
    this.state = {
      title: '',
      author: ''
    }

    this.onChange = this.onChange.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
  }

  onChange (e) {
    this.setState({ [e.target.name]: e.target.value })
  }

  onSubmit (e) {
    e.preventDefault()
    const post = {
      title: this.state.title,
      body: this.state.body
    }

    this.props.createBook(post)
  }

  render () {
    const { title, author } = this.state
    return (
      <div>
        <h1>Add Post</h1>
        <Form onSubmit={this.onSubmit}>
          <FormGroup>
            <Label>Title: </Label>
            <Input
              type='text'
              name='title'
              onChange={this.onChange}
              value={title}
            />
          </FormGroup>
          <FormGroup>
            <Label>Author: </Label>
            <Input
              type='textarea'
              name='author'
              onChange={this.onChange}
              value={author}
            />
          </FormGroup>
          <Button type='submit'>Submit</Button>
        </Form>
      </div>
    )
  }
}

BookForm.propTypes = {
  createBook: PropTypes.func.isRequired
}

export default connect(null, { createBook })(BookForm)
