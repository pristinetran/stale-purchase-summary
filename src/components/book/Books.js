import React from 'react'
import PropTypes from 'prop-types'
import { setActiveSorting } from '../../utils/index'
import { Button, Form, FormGroup, Label, Input, Table } from 'reactstrap'

const Books = (props) => {
  const { books, currentId, title, author, onSubmit, setCurrentRecordId, onTitleChange, onAuthorChange, handleClick, sortingDict } = props
  const onClick = (key, dir) => () => handleClick(key, dir)

  const onClickRecord = (id) => () => setCurrentRecordId(id)

  const bookItems = books.map((book, k) => (
    <tr key={book.title}>
      <td><a href='#' onClick={onClickRecord(k)}>{book.title}</a></td>
      <td>{book.author}</td>
      <td>{book.published}</td>
    </tr>
  ))

  const bookDetails = currentBook => (
    <tr>
      <td>{currentBook.title}</td>
      <td>{currentBook.reviewCount}</td>
      <td>{currentBook.published}</td>
      <td>{currentBook.publisher}</td>
    </tr>
  )

  return (
    <div>
      <Form onSubmit={onSubmit}>
        <FormGroup>
          <Label>Title Filter: </Label>
          <Input
            type='text'
            name='title'
            onChange={onTitleChange}
            defaultValue={title}
          />
        </FormGroup>
        <FormGroup>
          <Label>Author Filter: </Label>
          <Input
            type='text'
            name='author'
            onChange={onAuthorChange}
            defaultValue={author || ''}
          />
        </FormGroup>
        <Button type='submit' style={{ marginBottom: '3%' }}>Submit</Button>
      </Form>
      <div className={'ui-flex-box'}>
        <Table>
          <thead>
            <tr>
              <th>Title
                <i className={`fa fa-chevron-down pointerCursor ${setActiveSorting(sortingDict['title'], 'desc')}`}
                  onClick={onClick('title', 'desc')} />
                <i className={`fa fa-chevron-up pointerCursor ${setActiveSorting(sortingDict['title'], 'asc')}`}
                  onClick={onClick('title', 'asc')} />
              </th>
              <th>Author
                <i className={`fa fa-chevron-down pointerCursor ${setActiveSorting(sortingDict['author'], 'desc')}`}
                  onClick={onClick('author', 'desc')} />
                <i className={`fa fa-chevron-up pointerCursor ${setActiveSorting(sortingDict['author'], 'asc')}`}
                  onClick={onClick('author', 'asc')} />
              </th>
              <th>Published</th>
            </tr>
          </thead>
          <tbody>
            {bookItems}
          </tbody>
        </Table>
        {currentId !== undefined &&
        <Table style={{ marginLeft: '5%' }}>
          <thead>
            <tr>
              <th>Title</th>
              <th>ReviewCount</th>
              <th>Published</th>
              <th>Publisher</th>
            </tr>
          </thead>
          <tbody>
            {bookDetails(books[currentId])}
          </tbody>
        </Table>
        }
      </div>
    </div>
  )
}

Books.propTypes = {
  books: PropTypes.array,
  currentId: PropTypes.any,
  title: PropTypes.string,
  author: PropTypes.string,
  onSubmit: PropTypes.func,
  setCurrentRecordId: PropTypes.func,
  onTitleChange: PropTypes.func,
  onAuthorChange: PropTypes.func,
  handleClick: PropTypes.func,
  sortingDict: PropTypes.object
}

export default Books
