import React from 'react'
import Routes from './Routes'
import { Container } from 'reactstrap'

const Body = () => (<Container style={{ marginTop: '2%' }}>
  <Routes />
</Container>)

export default Body
