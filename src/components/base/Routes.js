import React from 'react'
import { Switch, Route } from 'react-router-dom'
import About from '../pages/About'
import Home from '../pages/Home'
import ContactUs from '../pages/ContactUs'
import Book from '../pages/Book'
import Agenda0 from '../pages/Agenda0'
import Agenda1 from '../../containers/agenda1'
import Agenda2Container from '../../containers/agenda2'
import Agenda3Container from '../../containers/agenda3'
import LifeCycle0 from '../pages/LifeCycle0'
import LifeCycle1Container from '../../containers/lifeCycle1'
import LifeCycle2Container from '../../containers/lifeCycle2'
import LifeCycle3Container from '../../containers/lifeCycle3'
import PurchaseSummaryContainer from '../../containers/checkout/purchaseSummary'

const Routes = () => (
  <main>
    <Switch>
      <Route exact path='/' component={Home} />
      <Route exact path='/Home' component={Home} />
      <Route exact path='/About' component={About} />
      <Route exact path='/ContactUs' component={ContactUs} />
      <Route exact path='/PurchaseSummary' component={PurchaseSummaryContainer} />
      <Route exact path='/BookStore' component={Book} />
      <Route exact path='/Agenda0' component={Agenda0} />
      <Route exact path='/Agenda1' component={Agenda1} />
      <Route exact path='/Agenda2' component={Agenda2Container} />
      <Route exact path='/Agenda3' component={Agenda3Container} />
      <Route exact path='/LifeCycle0' component={LifeCycle0} />
      <Route exact path='/LifeCycle1' component={LifeCycle1Container} />
      <Route exact path='/LifeCycle2' component={LifeCycle2Container} />
      <Route exact path='/LifeCycle3' component={LifeCycle3Container} />
    </Switch>
  </main>
)

export default Routes
