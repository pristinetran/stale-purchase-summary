import React from 'react'
import { Navbar, Nav, NavItem, NavLink, Dropdown, DropdownToggle, DropdownMenu } from 'reactstrap'

export default class NavBar extends React.Component {
  constructor (props) {
    super(props)

    this.toggleUseStateDropdown = this.toggleUseStateDropdown.bind(this)
    this.toggleUseEffectDropdown = this.toggleUseEffectDropdown.bind(this)
    this.state = {
      useStateDropdownOpen: false,
      useEffectDropdownOpen: false
    }
  }

  toggleUseStateDropdown () {
    this.setState(prevState => ({
      useStateDropdownOpen: !prevState.useStateDropdownOpen
    }))
  }

  toggleUseEffectDropdown () {
    this.setState(prevState => ({
      useEffectDropdownOpen: !prevState.useEffectDropdownOpen
    }))
  }

  render () {
    return (
      <div>
        <Navbar color='light' light expand='md'>
          <Nav className='ml-auto' navbar>
            <NavItem>
              <NavLink href='/Home'>Home</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href='/BookStore'>Book Store</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href='/About'>About</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href='/ContactUs'>Contact</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href='/PurchaseSummary'>Purchase Summary</NavLink>
            </NavItem>
            <Dropdown isOpen={this.state.useStateDropdownOpen} toggle={this.toggleUseStateDropdown} className={'ui-dropdown'}>
              <DropdownToggle caret>
                useState
              </DropdownToggle>
              <DropdownMenu>
                <NavItem>
                  <NavLink href='/Agenda0'>Agenda0 (original by React-Hooks suggestion)</NavLink>
                </NavItem>
                <NavItem>
                  <NavLink href='/Agenda1'>Agenda1 (with recompose)</NavLink>
                </NavItem>
                <NavItem>
                  <NavLink href='/Agenda2'>Agenda2 (Reduxless-React hook)</NavLink>
                </NavItem>
                <NavItem>
                  <NavLink href='/Agenda3'>Agenda3 (Redux-React hook)</NavLink>
                </NavItem>
              </DropdownMenu>
            </Dropdown>
            <Dropdown isOpen={this.state.useEffectDropdownOpen} toggle={this.toggleUseEffectDropdown}>
              <DropdownToggle caret>
                useEffect
              </DropdownToggle>
              <DropdownMenu>
                <NavItem>
                  <NavLink href='/LifeCycle0'>LifeCycle0 (original by React-Hooks suggestion)</NavLink>
                </NavItem>
                <NavItem>
                  <NavLink href='/LifeCycle1'>LifeCycle1 (with recompose)</NavLink>
                </NavItem>
                <NavItem>
                  <NavLink href='/LifeCycle2'>LifeCycle2 (Reduxless-React hook)</NavLink>
                </NavItem>
                <NavItem>
                  <NavLink href='/LifeCycle3'>LifeCycle3 (Redux-React hook)</NavLink>
                </NavItem>
              </DropdownMenu>
            </Dropdown>
          </Nav>
        </Navbar>
      </div>
    )
  }
}
