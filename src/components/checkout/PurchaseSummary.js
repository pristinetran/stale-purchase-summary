import React from 'react'
import PropTypes from 'prop-types'
import { Grid, Row, Col } from 'react-flexbox-grid'

const PurchaseSummary = (props) => {
  const { showItemDetails, setShowitemdetails, showPromoCode, setShowpromocode, promoCodeText, setPromocodetext, pricing, itemDetails, pickupSavingsText, applyDiscount } = props

  const onClickItemDetailsHandler = (e) => {
    e.preventDefault()
    setShowitemdetails(!showItemDetails)
  }

  const onClickPromoCodeHandler = (e) => {
    e.preventDefault()
    setShowpromocode(!showPromoCode)
  }

  const handleOnChange = (val) => {
    setPromocodetext(val)
  }

  const handleSubmit = (e) => {
    e.preventDefault()
    applyDiscount(promoCodeText)
  }

  const renderItemDetailsSection = () => (
    <div>
      <a href='#' onClick={e => onClickItemDetailsHandler(e)}>{showItemDetails ? 'Hide Item Details -' : 'See Item Details +'}</a>
      <Grid className={`${!showItemDetails ? 'ui-hide' : ''}`} fluid>
        <Row>
          <Col xs={2} md={2} lg={1}><img src={itemDetails.img} width='60' height='60' /></Col>
          <Col xs={3} md={3} lg={3}>
            <Row>{itemDetails.itemName}</Row>
            <Row>
              <Col><span><strong>${itemDetails.salePrice}</strong></span></Col>
              <Col xs={9} md={9}><span className='pull-right'>Qty: {itemDetails.quantity}</span></Col>
            </Row>
            <Row>
              <Col><span className='ui-regular-price'><strike>${itemDetails.regularPrice}</strike></span></Col>
            </Row>
          </Col>
        </Row>
      </Grid>
    </div>
  )

  const renderPromoCodeSection = () => (
    <div>
      <a href='#' onClick={e => onClickPromoCodeHandler(e)}>{showPromoCode ? 'Hide Promo Code -' : 'Apply Promo Code +'}</a>
      <section className={`${!showPromoCode ? 'ui-hide' : ''}`}>
        <input type='text' className='ui-text-input' onChange={e => handleOnChange(e.target.value)} />
        <input type='submit' className='ui-btn' onClick={e => handleSubmit(e)} value='Apply' />
      </section>
    </div>
  )

  return (
    <div className='ui-container'>
      <br/>
      <Grid fluid>
        <Row>
          <Col xs={3} md={3}>
            <Row>Subtotal</Row>
            <Row><a href='#' className='has-tooltip'>Pickup Savings<span className='tooltip tooltip-with-border' role='tooltip'>{pickupSavingsText}</span></a></Row>
            <Row>Est. taxes & fees<br />(Based on {pricing.zip})</Row>
          </Col>
          <Col xs={3} md={3}>
            <Row>${pricing.subtotal}</Row>
            <Row className='ui-pickup-savings'>-${pricing.savings}</Row>
            <Row>${pricing.tax}</Row>
          </Col>
        </Row>
      </Grid>
      <hr className='ui-divider' />
      <Grid fluid>
        <Row>
          <Col xs={3} md={3}>
            <Row><h5><strong>Est. total</strong></h5></Row>
          </Col>
          <Col xs={3} md={3}>
            <Row><h5><strong>${pricing.total}</strong></h5></Row>
          </Col>
        </Row>
      </Grid>
      {renderItemDetailsSection()}
      <hr className='ui-divider' />
      {renderPromoCodeSection()}
    </div>
  )
}

PurchaseSummary.propTypes = {
  showItemDetails: PropTypes.bool,
  setShowitemdetails: PropTypes.func,
  showPromoCode: PropTypes.bool,
  setShowpromocode: PropTypes.func,
  promoCodeText: PropTypes.string,
  setPromocodetext: PropTypes.func,
  pricing: PropTypes.object,
  itemDetails: PropTypes.object,
  pickupSavingsText: PropTypes.string,
  applyDiscount: PropTypes.func
}

export default PurchaseSummary
