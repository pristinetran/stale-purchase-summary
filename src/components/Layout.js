import React from 'react'
import { Container } from 'reactstrap'
import Header from './base/Header'
import Body from './base/Body'

const Layout = () => (
  <Container className='App'>
    <Header />
    <Body />
  </Container>
)

export default Layout
