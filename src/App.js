import React, { Component } from 'react'
import { Provider } from 'react-redux'
import store, { history } from './store'
import Layout from './components/Layout'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'font-awesome/css/font-awesome.min.css'
import './styles/scss/index.scss'
import { BrowserRouter } from 'react-router-dom'
import { ConnectedRouter } from 'react-router-redux'

class App extends Component {
  render () {
    async function asyncForEach (array, callback) {
      for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array)
      }
    }
    const waitFor = (ms) => new Promise((resolve, reject) => setTimeout(resolve, ms))
    const start = async () => {
      await asyncForEach([1, 2, 3], async (num) => {
        await waitFor(50)
        console.log(num)
      })
      console.log('Done')
    }
    start()

    return (
      <BrowserRouter>
        <Provider store={store}>
          <ConnectedRouter history={history}>
            <Layout />
          </ConnectedRouter>
        </Provider>
      </BrowserRouter>
    )
  }
}

export default App
