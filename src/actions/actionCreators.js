import {
  NEW_POST,
  SET_CURRENT_RECORD_ID,
  SET_FILTERS,
  SET_PRICING,
  SET_SORTING_DICT,
  SET_PROMO_CODE
} from './types'
import { fetchMethod } from '../utils'

export const fetchAllBooks = async (dispatch, getState) => {
  const URL = 'https://api.myjson.com/bins/x1m6k'
  const response = await fetchMethod(dispatch, 'BOOKS', { url: URL, method: 'GET' }) // eslint-disable-line
}

export const fetchProduct = async (dispatch, getState) => {
  const URL = 'http://localhost:7007/src/mockData/purchaseSummaryData.json'
  const response = await fetchMethod(dispatch, 'PRODUCT', { url: URL, method: 'GET' }) // eslint-disable-line
}

export const createBook = postData => dispatch => {
  fetch('https://jsonplaceholder.typicode.com/posts', {
    method: 'POST',
    headers: {
      'content-type': 'application/json'
    },
    body: JSON.stringify(postData)
  })
    .then(res => res.json())
    .then(post =>
      dispatch({
        type: NEW_POST,
        payload: post
      })
    )
}

export const setFilters = (payload) => dispatch => {
  dispatch({
    type: SET_FILTERS,
    payload
  })
}

export const setSortingDict = (payload) => dispatch => {
  dispatch({
    type: SET_SORTING_DICT,
    payload
  })
}

export const setCurrentRecordId = (payload) => dispatch => {
  dispatch({
    type: SET_CURRENT_RECORD_ID,
    payload
  })
}

export const setPricing = (payload) => dispatch => dispatch({
  type: SET_PRICING,
  payload
})

export const setPromoCode = (payload) => dispatch => dispatch({
  type: SET_PROMO_CODE,
  payload
})
