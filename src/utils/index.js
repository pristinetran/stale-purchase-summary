import reduce from 'lodash/reduce'
import orderBy from 'lodash/orderBy'
import axios from 'axios'
import { useState, useEffect } from 'react'
import merge from 'lodash/merge'
import capitalize from 'lodash/capitalize'

const hasString = (word, str) => word.toLowerCase().includes(str.toLowerCase())
export const getFilteredData = (data, title, author) =>
  reduce(data, (result, ele) => (hasString(ele.title, title) && hasString(ele.author, author)) ? [...result, ele] : result, [])

export const fetchMethod = async (dispatch, action = 'SOMETHING', options = {
  url: '',
  method: 'GET',
  data: {},
  body: {},
  params: {},
  headers: {},
  auth: {},
  timeout: 0,
  withCredentials: false,
  responseType: 'json',
  proxy: {}
}) => {
  let actions = action
  if (typeof action === 'string') {
    actions = {
      request: `FETCH_${action}_REQUEST`,
      success: `FETCH_${action}_SUCCESS`,
      failure: `FETCH_${action}_FAILURE`
    }
  }
  dispatch({ type: actions.request })
  try {
    const response = await axios(options)
    if (response.status < 200 || response.status > 299) {
      console.error(`Error ${actions.failure}`, options)
      dispatch({ type: actions.failure, error: `Error ${actions.failure}` })
    } else {
      dispatch({ type: actions.success, payload: response.data })
    }
    return response
  } catch (error) {
    dispatch({ type: actions.failure, error })
  }
}

export const sortData = (originalData, sortingDict) => {
  const keyArr = []
  const dirArr = []

  Object.keys(sortingDict).map(x => {
    if (sortingDict[x]) {
      keyArr.push(x)
      dirArr.push(sortingDict[x])
    }
  })

  return orderBy(originalData, keyArr, dirArr)
}

export const withStateHandlers = (initialState = {}, addCustomHandler, props = {}) => {
  const enhancedUseState = reduce(initialState, (result, ele, key) => {
    const arr = useState(initialState[key])
    // console.warn('key >>>', key)
    return key ? merge(result, { [key]: arr[0], [`set${capitalize(key)}`]: arr[1] }) : result
  }, {})

  const mergedProps = { ...props, ...enhancedUseState }
  const customHandler = addCustomHandler && typeof addCustomHandler === 'function' ? addCustomHandler(enhancedUseState, mergedProps) : {}

  return { ...mergedProps, ...customHandler }
}

export const sampleGetBooks = async (setBooks) => {
  const URL = 'https://api.myjson.com/bins/x1m6k'
  const options = { url: URL, method: 'GET' }
  const response = await axios(options)
  setBooks(response.data)
}

export const lifecycle = (lambda, observables = []) => { useEffect(lambda, observables) }

export const compose = (...funcs) => x => funcs.reduceRight((composed, f) => f(composed), x)

export const setActiveSorting = (val, dir) => (val === dir) && 'ui-red'
