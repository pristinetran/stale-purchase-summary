import { lifecycle, sampleGetBooks, withStateHandlers } from '../utils'
import LifeCycle2 from '../components/pages/LifeCycle2'

import isEmpty from 'lodash/isEmpty'

// define your initial state
const initialState = {
  books: []
}

// addState wrapper role is to
// 1) provide an enhanced version of the useState and all custom handlers
// 2) wrapping the end Component where you want your connector to connect to
const LifeCycle2Container = (props) => {
  const allProps = withStateHandlers(initialState, props)

  lifecycle(() => {
    const { books, setBooks } = allProps
    if (isEmpty(books)) {
      sampleGetBooks(setBooks)
    }
  })

  return LifeCycle2(allProps)
}

export default LifeCycle2Container
