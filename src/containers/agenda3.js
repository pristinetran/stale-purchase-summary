/// Redux use case for using React hook *useState* in the Container
import { withStateHandlers } from '../utils'
import Agenda3 from '../components/pages/Agenda3'
import { connect } from 'react-redux'

// assume you have Redux state and some Redux dispatchers
const mapStateToProps = (state, oProps) => {
  const { books: { items } } = state
  return { xyz: 'This variable xyz is from mapStateToProps', items }
}
const mapDispatchToProps = (dispatch, oProps) => ({})
// define your initial state
const initialState = {
  count: 0,
  step: 1,
  profile: {
    firstName: 'John',
    lastName: 'Doe',
    age: 36
  }
}

// added some custom handlers
const addHandlers = (state, props) => ({
  onAdd: () => {
    console.warn('accessibility to the enhancedUseState state?', state)
    console.warn('accessibility to all props?', props)
    return state.setCount(c => c + state.step)
  }
})

// addState wrapper role is to
// 1) provide an enhanced version of the useState and all custom handlers
// 2) wrapping the end Component where you want your connector to connect to
const addState = (props) => {
  const allProps = withStateHandlers(initialState, addHandlers, props)
  console.warn('allProps >>>', allProps)

  return Agenda3(allProps)
}

const connector = connect(mapStateToProps, mapDispatchToProps)
export default connector(addState)
