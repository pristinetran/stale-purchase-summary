/// Reduxless use case for using React hook *useState* in the Container
import { withStateHandlers } from '../utils'
import Agenda2 from '../components/pages/Agenda2'

// define your initial state
const initialState = {
  count: 0,
  step: 1,
  profile: {
    firstName: 'John',
    lastName: 'Doe',
    age: 36
  }
}

// added some custom handlers
const addHandlers = (state, props) => ({
  onAdd: () => {
    console.warn('accessibility to the enhancedUseState state?', state)
    console.warn('accessibility to all props?', props)
    return state.setCount(c => c + state.step)
  }
})

// addState wrapper role is to
// 1) provide an enhanced version of the useState and all custom handlers
// 2) wrapping the end Component where you want your connector to connect to
const Agenda2Container = (props) => {
  /// imported and used new wrapper function of withStateHandlers which in turn handles all useState invoking
  /// and ease developers from not having to worry about the syntax gutter and verbosity of useState
  const allProps = withStateHandlers(initialState, addHandlers, props)
  console.warn('allProps >>>', allProps)

  return Agenda2(allProps)
}

export default Agenda2Container
