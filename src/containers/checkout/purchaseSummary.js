import React from 'react'
import { connect } from 'react-redux'
import PurchaseSummary from '../../components/checkout/PurchaseSummary'
import { fetchProduct, setPricing, setPromoCode } from '../../actions/actionCreators'
import reduce from 'lodash/reduce'
import { lifecycle, withStateHandlers } from '../../utils/index'
import { PICK_UP_SAVING_TEXT } from '../../constants/index'

const mapStateToProps = ({ purchaseSummary: { isFetching, promoCode, product } }) => {
  const { pricing = {}, itemDetails = {} } = product
  return { isFetching, pricing, itemDetails, promoCode, pickupSavingsText: PICK_UP_SAVING_TEXT }
}

const mapDispatchToProps = (dispatch) => {
  return {
    setPricing: (payload) => dispatch(setPricing(payload)),
    setPromoCode: (payload) => dispatch(setPromoCode(payload)),
    fetchProduct: () => dispatch(fetchProduct)
  }
}

const mergeProps = (sProps, dProps, oProps) => {
  const { pricing, promoCode } = sProps
  const { setPricing, setPromoCode } = dProps

  const applyDiscount = (promoCodeInput) => {
    const discountEles = ['total', 'tax', 'subtotal']
    const validPromoCodes = ['DISCOUNT']
    if (promoCode !== promoCodeInput) {
      setPromoCode(promoCodeInput)
      if (validPromoCodes.includes(promoCodeInput)) {
        const obj = (reduce(pricing, function (result, value, key) {
          result = discountEles.includes(key) ? { ...result, [key]: (value * 0.9).toFixed(2) } : { ...result, [key]: value }
          return result
        }, {}))
        setPricing(obj)
      }
    }
  }

  return {
    ...sProps,
    ...dProps,
    applyDiscount
  }
}

const initialState = {
  showItemDetails: false,
  showPromoCode: false,
  promoCodeText: ''
}

const addState = (props) => {
  const allProps = withStateHandlers(initialState, {}, props)

  const { pricing, itemDetails, isFetching } = allProps

  lifecycle(() => {
    if (Object.keys(pricing).length < 1 || Object.keys(itemDetails).length < 1) {
      allProps.fetchProduct()
    }
  })

  return (Object.keys(pricing).length < 1 || Object.keys(itemDetails).length < 1 || isFetching) ? <span /> : PurchaseSummary(allProps)
}

const connector = connect(mapStateToProps, mapDispatchToProps, mergeProps)
export default connector(addState)
