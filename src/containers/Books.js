import { connect } from 'react-redux'
import Books from '../components/book/Books'
import { getFilteredData, sortData } from '../utils'
import omit from 'lodash/omit'

const mapStateToProps = (state, oProps) => {
  const {
    currentId,
    books: { filterOptions: { titleFilter, authorFilter }, sortingDict }
  } = state

  const data = titleFilter || authorFilter ? getFilteredData(oProps.books, titleFilter, authorFilter) : oProps.books
  const sortedData = sortData(data, sortingDict)
  return { ...oProps, books: sortedData, titleFilter, authorFilter, sortingDict, currentId }
}

const mapDispatchToProps = (dispatch, oProps) => (omit(oProps, 'books'))

export default connect(mapStateToProps, mapDispatchToProps)(Books)
