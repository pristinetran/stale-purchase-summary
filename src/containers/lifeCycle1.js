import { compose, lifecycle, withStateHandlers } from 'recompose'
import LifeCycle1 from '../components/pages/LifeCycle1'
import { connect } from 'react-redux'
import axios from 'axios/index'
import isEmpty from 'lodash/isEmpty'

const mapStateToProps = (state, oProps) => {
  return {}
}

const mapDispatchToProps = (dispatch, oProps) => ({})

const addState = withStateHandlers(
  {
    books: []
  },
  {
    setBooks: (state, props) => (data) => {
      return { books : data }
    }
  }
)

const addLife = lifecycle({
  async componentDidMount () {
    if (isEmpty(this.props.books)) {
      const URL = 'https://api.myjson.com/bins/x1m6k'
      const options = { url: URL, method: 'GET' }
      const response = await axios(options)
      this.props.setBooks(response.data)
    }
  }
})

const connector = connect(mapStateToProps, mapDispatchToProps)
const enhance = compose(connector, addState, addLife)
export default enhance(LifeCycle1)
