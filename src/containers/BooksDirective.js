import { connect } from 'react-redux'
import { fetchAllBooks, setCurrentRecordId, setFilters, setSortingDict } from '../actions/actionCreators'
import Books from './Books'
import { branch, lifecycle, renderNothing, compose, withStateHandlers } from 'recompose'
import querystring from 'querystring'

const mapStateToProps = state => {
  const {
    items,
    filterOptions
  } = state.books

  console.log('Dir State', state)

  return {
    ...state.books,
    books: items,
    filterOptions
  }
}

const mapDispatchToProps = (dispatch) => {
  const fetching = () => dispatch(fetchAllBooks)

  return {
    fetching,
    setFilterOptions: (payload) => dispatch(setFilters(payload)),
    updateSortingDict: (payload) => dispatch(setSortingDict(payload)),
    setCurrentRecordId: (payload) => dispatch(setCurrentRecordId(payload))
  }
}

const whenNothing = branch(
  ({ books }) => books.length < 1,
  renderNothing
)

const addState = withStateHandlers(
  {
    title: '',
    author: ''
  },
  {
    init: (state, props) => () => ({ title : props.filterOptions.titleFilter, author: props.filterOptions.authorFilter }),
    onTitleChange: () => (e) => ({ title: e.target.value }),
    onAuthorChange: () => (e) => ({ author: e.target.value }),
    onSubmit: (state, props) => (e) => {
      e.preventDefault()
      const { title, author } = state
      props.setFilterOptions({ titleFilter: title, authorFilter: author })
      window.history.replaceState('', '', `/BookStore?t=${title}&a=${author}`)
    },
    handleClick: (state, props) => (key, dir) => {
      props.sortingDict[key] === dir ? props.updateSortingDict({ [key] : undefined }) : props.updateSortingDict({ [key] : dir })
    }
  }
)

const addLife = lifecycle({
  componentDidMount () {
    if (window.location.search) {
      this.props.init()
      const qs = querystring.parse(window.location.search.replace('?', ''))
      this.props.setFilterOptions({ authorFilter: qs.a, titleFilter: qs.t })
    }
    this.props.fetching()
  }
})

const connector = connect(mapStateToProps, mapDispatchToProps)
const enhance = compose(connector, addState, addLife, whenNothing)

export default enhance(Books)
