import { connect } from 'react-redux'
import Agenda from '../components/pages/Agenda1'
import { withStateHandlers, compose } from 'recompose'
import merge from 'lodash/merge'

const mapStateToProps = (state, oProps) => {
  return {}
}

const mapDispatchToProps = (dispatch, oProps) => ({})

/// The good, old recompose use
const addState = withStateHandlers(
  {
    count: 0,
    step: 1,
    profile: {
      firstName: 'John',
      lastName: 'Doe',
      age: 36
    }
  },
  {
    onAdd: (state, props) => () => ({
      count: state.count + state.step
    }),
    setProfile: (state) => (e, obj) => {
      e.preventDefault()
      const key = Object.keys(obj)[0]
      if (key) {
        return merge(state.profile, {
          [key]: e.target.value
        })
      }

      return {}
    }
  }
)

const connector = connect(mapStateToProps, mapDispatchToProps)
const enhance = compose(connector, addState)

export default enhance(Agenda)
