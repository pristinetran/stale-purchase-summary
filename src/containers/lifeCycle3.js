import { lifecycle, withStateHandlers } from '../utils'
import LifeCycle3 from '../components/pages/LifeCycle3'
import { connect } from 'react-redux'
import { fetchAllBooks } from '../actions/actionCreators'

// assume you have Redux state and some Redux dispatchers
const mapStateToProps = (state, oProps) => {
  const { books: { items } } = state
  return { xyz: 'This variable xyz is from mapStateToProps', items }
}
const mapDispatchToProps = (dispatch, oProps) => ({ fetchAllBooks: () => dispatch(fetchAllBooks) })

// define your initial state
const initialState = {}

// added some custom handlers
const addHandlers = (state, props) => ({})

// addState wrapper role is to
// 1) provide an enhanced version of the useState and all custom handlers
// 2) wrapping the end Component where you want your connector to connect to
const addState = (props) => {
  const allProps = withStateHandlers(initialState, addHandlers, props)
  console.warn('allProps >>>', allProps)

  lifecycle(() => {
    if (allProps.items.length < 3) {
      allProps.fetchAllBooks()
    }
  })

  return LifeCycle3(allProps)
}

const connector = connect(mapStateToProps, mapDispatchToProps)
export default connector(addState)
