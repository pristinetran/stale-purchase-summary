# POC project:
- Achieved the goal of eliminating the use of the deprecated life-cycle hook named componentWillReceiveProps(),
 with dynamic rendition through simple set of containers and components. componentDidMount alone is sufficient for Books module.
- Sample implementation of Redux with React.
- The goal of PoC implementation of withStateHandlers and lifecycle methods in utils/index.js is to keep it simple, easy to do, less fragmented code, easy code migration and maintenance.
  The original suggestion from React Hooks documents and reference article seem to be bundling everything at Component level and the implemented code becomes very scattered and fragmented.
  with multiple useState(s), multiple useEffect(s), and handlers hanging in the Component, adding distraction, technical overhead, and review effort on the Component level with more gimmicky monolithic approach.

# Quick Start

## Install dependencies
- with yarn (recommended): `yarn`, if you don\'t have yarn, you can do `npm i -g yarn`
- with npm: `npm install`

## Run a local server
- with yarn: `yarn serve`
- with npm: `npm run serve`

## Serve app on localhost:3000
- with yarn: `yarn start`
- with npm: `npm start`

# Highlighted technologies:
- React v16.8.x and React Hooks
- Utils for React Hooks: withStateHandlers for multiple occurrences of useState, lifecycle for useEffect
- Redux, Redux-Redux, React-Router, BrowserHistory.
- fetchMethod is a handy wrapper on top of regular axios

Note

If you use this optimization, make sure the array includes all values from the component scope (such as props and state) that change over time and that are used by the effect. Otherwise, your code will reference stale values from previous renders. Learn more about how to deal with functions and what to do when the array changes too often.

If you want to run an effect and clean it up only once (on mount and unmount), you can pass an empty array ([]) as a second argument. This tells React that your effect doesn’t depend on any values from props or state, so it never needs to re-run. This isn’t handled as a special case — it follows directly from how the inputs array always works.

If you pass an empty array ([]), the props and state inside the effect will always have their initial values. While passing [] as the second argument is closer to the familiar componentDidMount and componentWillUnmount mental model, there are usually better solutions to avoid re-running effects too often. Also, don’t forget that React defers running useEffect until after the browser has painted, so doing extra work is less of a problem.

We recommend using the exhaustive-deps rule as part of our eslint-plugin-react-hooks package. It warns when dependencies are specified incorrectly and suggests a fix.


Refs:
1) https://reactjs.org/docs/hooks-effect.html
2) https://www.robinwieruch.de/react-hooks-fetch-data/
